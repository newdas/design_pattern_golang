package memento

import (
	"testing"
)

func TestMemento(t *testing.T) {
	ming := NewPerson("ming", 18, 180, 50)
	ming.Print()

	memo := ming.Bak()
	controller := NewTimeController()
	controller.Save(18, memo)

	ming.GrowUp(100, 190, 40)
	ming.Print()

	ming.Renewal(controller.GetMemo(18))
	ming.Print()
}