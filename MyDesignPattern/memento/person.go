package memento

import (
	"fmt"
)

type person struct {
	name string
	age int
	height int
	weight int
}

func (person *person) GetAge() int {
	return person.age
}

func (person *person) GrowUp(age, height, weight int) {
	person.age = age
	person.height = height
	person.weight = weight
}

func (person *person) Bak() memo {
	memo := memo{person.age, person.height, person.weight}
	return memo
}

func (person *person) Renewal(memo memo){
	person.age = memo.age
	person.height = memo.height
	person.weight = memo.weight
}


func (person *person) Print() {
	fmt.Printf("I am %s, i am %d years old, i am %d meter tall and weigh %d kilogram.\n", person.name, person.age, person.height, person.weight)
}

func NewPerson(name string, age, height, weight int) *person  {
	person := &person{name, age, height, weight}
	return person
}
