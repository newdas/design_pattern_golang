package facade

import (
	"testing"
)

func TestFacade(t *testing.T) {
	coffee := NewCoffee()
	coffee.Brew()
}