package composite

import (
	"testing"
)

func TestComposite(t *testing.T) {
	solderA := Soldier{"soldierA"}
	solderB := Soldier{"soldierB"}
	solderC := Soldier{"soldierC"}
	solderD := Soldier{"soldierD"}

	soldier_queue_A := []*Soldier{&solderA, &solderB}
	captainA := Captain{"captainA", soldier_queue_A}
	soldier_queue_B := []*Soldier{&solderC, &solderD}
	captainB := Captain{"captainB", soldier_queue_B}

	captain_queue := []*Captain{&captainA, &captainB}
	general := General{"GENERAL", captain_queue}
	general.Fight()
}