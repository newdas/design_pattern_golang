package iterator

import (
	"fmt"
	"testing"
)

func TestIterator(t *testing.T) {
	horses := []string{"white", "black", "brown"}
	horses_queue := NewHorses(horses)

	soldiers := map[string] string {
		"zhangsan": "zhangsan",
		"lisi": "lisic",
		"wangwu": "wangwu",
	}
	soldiers_queue := NewSoldiers(soldiers)

	fmt.Println(horses_queue.Count(), horses_queue.Next())
	fmt.Println(soldiers_queue.Count(), soldiers_queue.Next())
	horses_queue.Remove()
	soldiers_queue.Remove()

	fmt.Println(horses_queue.Count(), horses_queue.Next())
	fmt.Println(soldiers_queue.Count(), soldiers_queue.Next())
}