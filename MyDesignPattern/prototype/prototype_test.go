package prototype

import (
	"fmt"
	"testing"
)

func TestPrototype(t *testing.T) {
	guard_sample := NewHonorGuard(18, 18, 18, "m")

	zhangsan := guard_sample.Clone()
	zhangsan.SetName("zhangsan")
	fmt.Println(zhangsan)

	lisi := guard_sample.Clone()
	lisi.SetName("lisi")
	fmt.Println(lisi)
}