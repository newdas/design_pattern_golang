package builder

import (
	"testing"
)

func TestBuilder(t *testing.T) {
	NewBuilder(ChineseType{}).Build()

	NewBuilder(ItalianType{}).Build()
}