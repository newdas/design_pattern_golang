package interpreter

import (
	"fmt"
	"testing"
)

func TestNewWords(t *testing.T) {
	hello := NewWords("hello")
	greeting := NewVariable("greeting")
	context := NewContext()
	context.AddItem("greeting", hello)
	context.AddItem("test", greeting)

	var_test := NewVariable("$$test")
	fmt.Println(var_test.Interpret(context))
}