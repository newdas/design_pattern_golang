package visitor

import (
	"fmt"
	"testing"
)

func TestVisitor(t *testing.T) {
	cart := NewShoppingCart()

	milk := NewGoods("milk", 10)
	milk.AddToCart(cart)

	desk := NewGoods("desk", 100)
	desk.AddToCart(cart)

	computer := NewGoods("computer", 8000)
	computer.AddToCart(cart)

	printer := NewPrinter()
	scanner := NewScanner()

	fmt.Println(printer.VisitGoods(cart))
	fmt.Println(scanner.VisitGoods(cart))
}