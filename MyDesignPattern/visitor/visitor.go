package visitor

type visitor interface {
	VisitGoods(cart *shoppingCart) string
}