package strategy

import (
	"fmt"
)

type IAlg interface {
	used()
}

type BubbleSort struct {
	name string
}

func NewBubbleSearch() BubbleSort {
	search := BubbleSort{"bubble sort"}
	return search
}

func (alg BubbleSort) used() {
	fmt.Println("I am using util " + alg.name)
}

type QuickSort struct {
	name string
}

func NewQuickSearch() QuickSort {
	search := QuickSort{"quick sort"}
	return search
}

func (alg QuickSort) used() {
	fmt.Println("I am using util " + alg.name)
}