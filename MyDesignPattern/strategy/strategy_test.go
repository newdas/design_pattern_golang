package strategy

import (
	"testing"
)

func TestCalculator_Cal(t *testing.T) {
	cal := NewCalculator()
	cal.Cal()

	cal.ChangeAlg(NewQuickSearch())
	cal.Cal()
}