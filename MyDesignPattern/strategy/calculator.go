package strategy

type calculator struct {
	alg IAlg
}

func NewCalculator() calculator {
	alg := NewBubbleSearch()
	calculator := calculator{alg:alg}
	return calculator
}

func (calculator * calculator) ChangeAlg(alg IAlg) {
	calculator.alg = alg
}

func (calculator calculator) Cal() {
	calculator.alg.used()
}