package state

import (
	"testing"
)

func TestNewFruit(t *testing.T) {
	fruit := NewFruit()
	fruit.harvest() // The plant is just planted！
	fruit.water()   // The plant is growing!
	fruit.harvest() // The plant is blooming, don't do that！
	fruit.water()   // The plant is ripping!
	fruit.water()   // The plant don't need water!
	fruit.harvest() // You got lots of fruits！
}