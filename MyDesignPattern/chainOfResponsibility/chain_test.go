package chainOfResponsibility

import (
	"fmt"
	"testing"
)

func TestChain(t *testing.T) {
	CEO := NewAuditor("CEO", 10000, nil)
	leader := NewAuditor("leader", 5000, CEO)
	manager := NewAuditor("manager", 1000, leader)

	fmt.Println(manager.Audit(500))
	fmt.Println(manager.Audit(3000))
	fmt.Println(manager.Audit(8000))
	fmt.Println(manager.Audit(11000))
}