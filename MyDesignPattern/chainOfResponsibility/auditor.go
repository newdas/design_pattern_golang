package chainOfResponsibility

type auditor struct {
	name string
	authority int
	superior *auditor
}

func (auditor auditor) Audit(request int) string {
	if request <= auditor.authority {
		return auditor.name + " permitted@"
	} else {
		if auditor.superior == nil {
			return "out of limited!"
		} else {
			return auditor.superior.Audit(request)
		}
	}
}

func NewAuditor(name string, authority int, superior *auditor) *auditor {
	auditor := &auditor{name, authority, superior}
	return auditor
}
