package template

import (
	"testing"
)

func TestCommunicate_SendMSg(t *testing.T) {
	cm := NewCommunicate("A", "hello", "B")

	cm.SendMSg(Sms{})

	cm.SendMSg(Mail{})
}