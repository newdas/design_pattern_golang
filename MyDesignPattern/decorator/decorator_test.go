package decorator

import (
	"fmt"
	"testing"
)

func TestDecorator(t *testing.T) {
	flower := NewGift("flower")
	fmt.Println(flower.GetDesc())

	flower_packed := NewPack(flower, "red ribbon")
	fmt.Println(flower_packed.GetDesc())

	flower_packed_packed := NewPack(flower_packed, "box")
	fmt.Println(flower_packed_packed.GetDesc())
}