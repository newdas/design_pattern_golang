package bridge

import (
	"testing"
)

func TestBridge(t *testing.T) {
	pencil := Pencil{}
	ballPen := BallPen{}

	RedPaper{pencil}.Paint()
	BluePaper{ballPen}.Paint()
}