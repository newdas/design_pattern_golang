package flyweight

import (
	"testing"
)

func TestFlyWeight(t *testing.T) {
	treeA := map[string]int{"height": 20}
	treeB := map[string]int{"height": 14}
	treeC := map[string]int{"height": 25}
	treeD := map[string]int{"height": 7}
	treeE := map[string]int{"height": 40}

	trees :=[]map[string]int { treeA, treeB, treeC, treeD, treeE}

	forest := NewForest(trees)
	forest.Fell()
}