package singleton

import (
	"sync"
)

var object *instance

var lock sync.Mutex

type instance struct {
	name string
}

func (instance instance) GetName() string {
	return instance.name
}

func (instance *instance) SetName(name string) {
	lock.Lock()
	defer lock.Unlock()

	instance.name = name
}

// todo 后续考虑并行问题
func GetInstance() *instance {
	lock.Lock()
	if object == nil {
		object = &instance{"test"}
	}
	lock.Unlock()

	return object
}