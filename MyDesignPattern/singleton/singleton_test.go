package singleton

import (
	"fmt"
	"testing"
)

func TestSingleton(t *testing.T) {
	ins1 := GetInstance()
	ins2 := GetInstance()
	fmt.Println(ins1 == ins2)

	ins2.SetName("ins2")
	fmt.Println(ins1 == ins2)
	fmt.Println(ins1.GetName(), ins2.GetName())
}