package factory

import (
	"testing"
)

func TestAdidasCottonShoe_Wear(t *testing.T) {
	lining_shoes_factory := LiNingFactory{}
	lining_shoe := lining_shoes_factory.Product("plastic")
	lining_shoe.Wear() // a lining plastic shoe, it's price is 120元;

	adidas_shoe_factory := AdidasFactory{}
	adidas_shoe := adidas_shoe_factory.Product("sports")
	adidas_shoe.Wear() // a adidas sports shoe, it's price is 70元;
}