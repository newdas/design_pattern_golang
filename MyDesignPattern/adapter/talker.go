package adapter

import (
	"fmt"
)

type Talker interface {
	Express()
}

type Person struct {
	Word string
}

func (person Person) Express() {
	fmt.Println(person.Word)
}

