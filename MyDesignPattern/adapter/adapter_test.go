package adapter

import (
	"testing"
)

func TestAdapter(t *testing.T) {
	var talker Talker

	talker = NoteBook{User:Mute{"hello i am notebook"}}
	talker.Express()

	talker = Person{"hello i am person"}
	talker.Express()
}