package proxy

import (
	"testing"
)

func TestProxy(t *testing.T) {
	postman := NewLazyPostman()
	postman.RequestLetter()
	postman.DemandLetter()
}