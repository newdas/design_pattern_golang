package mediator

import (
	"fmt"
	"testing"
)

func TestMediator(t *testing.T) {
	france := NewCountry("France")
	korea := NewCountry("Korea")

	unitedNations := NewUnitedNations(france, korea)

	france.Announce("I am a romantic country.")
	fmt.Println(korea.Receive(unitedNations))

	korea.Announce("I am good at copy.")
	fmt.Println(france.Receive(unitedNations))
}