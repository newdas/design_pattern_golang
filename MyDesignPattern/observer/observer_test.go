package observer

import (
	"testing"
)

func TestObserver(t *testing.T) {
	news := News{}

	zhangsan := NewNewsReporter("zhangsan")
	news.Focus(zhangsan)

	lisi := NewNewsReporter("lisi")
	news.Focus(lisi)

	news.Happen("someone was killed!")

	news.Ignore(lisi)
	news.Happen("a building built in the city!")
}