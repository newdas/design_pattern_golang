package observer

import (
	"fmt"
)

// 记者接口，有报道方法
type IReporter interface {
	report(event string)
}

// 新闻记者基类，有名字属性，实现报道方法
type newsReporter struct {
	name string
}

func (reporter newsReporter) report(event string) {
	fmt.Println(reporter.name + " reported an event: " + event)
}

func NewNewsReporter(name string) newsReporter {
	reporter := newsReporter{name: name}
	return reporter
}



