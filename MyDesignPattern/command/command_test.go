package command

import (
	"testing"
)

func TestCommand(t *testing.T) {
	soldier := Soldier{Name:"liuda"}

	run := Run{Start:"start", Destination:"destination"}
	soldier.ExecCommand(run)
	soldier.UndoCommand(run)

	attention := Attention{}
	soldier.ExecCommand(attention)
	soldier.UndoCommand(attention)
}